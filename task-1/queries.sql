/*
Структура таблицы
 */

CREATE TABLE IF NOT EXISTS `Users` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`login` varchar(128) DEFAULT NULL,
`fio` varchar(255) DEFAULT NULL,
`pass` varchar(32) NOT NULL DEFAULT '202cb962ac59075b964b07152d234b70',
PRIMARY KEY (`id`),
KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

INSERT INTO `Users` (`id`, `login`, `fio`, `pass`) VALUES
(1, 'login-1', 'Иванов Иван', '202cb962ac59075b964b07152d234b70'),
(2, 'login-1', 'Иванов Иван 2', '202cb962ac59075b964b07152d234b70'),
(3, 'fedorov', 'Федоров', '202cb962ac59075b964b07152d234b70'),
(4, 'nick', 'Ковенько Николай', '202cb962ac59075b964b07152d234b70'),
(5, 'nick', 'Ковенько Николай Александрович', '202cb962ac59075b964b07152d234b70'),
(6, 'nick', 'Ковенько Н. А.', '202cb962ac59075b964b07152d234b70'),
(7, 'login-3', 'Тестовый пользователь', '202cb962ac59075b964b07152d234b70');


/*
Запрос на выборку
 */

select `login`, count(*) as `l_count`
from `Users`
group by `login`
having `l_count`>1
order by `l_count` desc