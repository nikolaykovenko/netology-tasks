<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

class controllers_factory {
 
 /**
  * @var string директория классов конроллеров
  */
 protected $controllers_dir;
 

 /**
  * Конструктор
  */
 public function __construct()
 {
  $this->controllers_dir = __DIR__.'/file_controllers/';
  require_once $this->controllers_dir.'a_file_controller.php';
 }

 /**
  * @param $filename
  * @return mixed
  * @throws Exception
  */
 public function get_controller($filename)
 {
  $info = pathinfo($filename);
  $ext = strtolower($info['extension']);
  
  $class_name = $ext.'_file_controller';
  
  if (file_exists($file = $this->controllers_dir.$class_name.'.php'))
  {
   require_once $file;
   if (class_exists($class_name)) return new $class_name();
  }
  
  throw new Exception('Controller "'.$class_name.'" not found');
 }
}