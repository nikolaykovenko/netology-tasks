<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

/**
 * Имя файла, который будет обрабатываться
 */
$filename = 'some_dir/some_file.txt';

require_once __DIR__.'/controllers_factory.php';

try {
 $factory = new controllers_factory();
 $controller = $factory->get_controller($filename);
 echo $controller->execute();
}
catch (Exception $e)
{
 echo $e->getMessage();
}

?>