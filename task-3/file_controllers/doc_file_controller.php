<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

/**
 * Обработчик DOC файлов
 */
class doc_file_controller extends a_file_controller {
 
 /**
  * Логика выполнения
  * @return string
  */
 public function execute()
 {
  return 'DOC files controller';
 }
}