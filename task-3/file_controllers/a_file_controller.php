<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */


/**
 * Абстрактный класс, предок всех файловых контроллеров
 * Class a_file_controller
 */
abstract class a_file_controller {

 /**
  * Логика выполнения
  * @return string
  */
 abstract public function execute();

 /**
  * Выполнение неких действий, которые нужно выполнять файла любого типа. Например генерирование футера, примечание, etc.
  * @return bool
  */
 protected function some_method()
 {
  return TRUE;
 }
}