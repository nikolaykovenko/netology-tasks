<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

/**
 * Обработчик текстовых файлов
 */
class txt_file_controller extends a_file_controller {

 /**
  * Логика выполнения
  * @return string
  */
 public function execute()
 {
  return 'TXT files controller';
 }
}