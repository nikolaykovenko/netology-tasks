function validation_fail(e, message)
{
 e.preventDefault();
 alert(message);
}

function check_email_exist(email)
{
 var result = null;
 
 $.ajax({
  type: 'post',
  url: 'index.php?mode=check_email_exist',
  data: 'email=' + email,
  async: false,
  success: function(data) {
   var response = $.parseJSON(data);
   result = response.status == 'ok';
  }
 });
 
 if (result) return result; else return false;
}

$(function() {
 $('.main-form').submit(function(e) {

  var email = $(this).find('[name="email"]'),
      body = $(this).find('[name="body"]'),
      tags = body.val().match(/<[^>]+>/i);
  
  if (tags) validation_fail(e, validation_messages['tags_not_permitted']);
  else
  {
   if (!check_email_exist(email.val())) validation_fail(e, validation_messages['email_exist']);
  }
 });
});