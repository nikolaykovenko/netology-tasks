<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

$validation_messages = array(
 'required'=>'Пожалуйста, заполните все обязательные поля',
 'email_error'=>'Пожалуйста, введите корректный email',
 'tags_not_permitted'=>'Извините, вводить теги не разрешается',
 'email_exist'=>'Данный Email уже зарегистрирован',
 'unknown_error'=>'Неизвестная ошибка'
);


if (!empty($_POST))
{
 require_once __DIR__.'/form_validator.php';
 require_once __DIR__.'/../db_config.php';
 $validator = new form_validator($db_config);
 $validate_all = !($_GET['mode'] == 'check_email_exist');
 
 try {
  if ($validate_all) $validator->validate($_POST['email'], $_POST['body']); else $validator->check_email_exist($_POST['email']);
  $result = array('status'=>'ok', 'message'=>'Форма успешно прошла валидацию');
 }
 catch (Exception $e)
 {
  $message = $e->getMessage();
  $result = array('status'=>'error', 'message'=>array_key_exists($message, $validation_messages) ? $validation_messages[$message] : $message);
 }
 
 if (!$validate_all)
 {
  echo json_encode($result);
  die();
 }
}
else $validation_result = NULL;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
 <meta charset="utf-8">
 <title>Тестовое задание 4 - netology</title>
 <script src="/js/jquery-1.10.2.min.js"></script>
 <script src="js/validation.js"></script>
 <script>
  var validation_messages = <?php echo json_encode($validation_messages); ?>;
 </script>
 <!-- Latest compiled and minified CSS -->
 <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
 <!-- Latest compiled and minified JavaScript -->
 <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container main-container">
 <div class="col-md-6 col-md-offset-3">
  <?php
  if (is_array($result)) echo '<div class="alert alert-'.($result['status'] == 'ok' ? 'success' : 'danger').'">'.$result['message'].'</div>';
  ?>
  <form class="main-form" role="form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" id="main-form" method="post">
   <div class="form-group">
    <label for="email">Email</label>
    <input type="email" name="email" class="form-control" id="email" placeholder="" value="<?php echo htmlspecialchars($_POST['email']); ?>" required>
   </div>
   <div class="form-group">
    <label for="body">Текст</label>
    <textarea class="form-control" name="body" id="body" required><?php echo $_POST['body']; ?></textarea>
   </div>
   <button type="submit" class="btn btn-default">Отправить</button>
  </form>
 </div>
</div>
</body>
</html>