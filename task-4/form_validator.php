<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

class form_validator {
 
 protected $db_config;
 
 protected $mysqli;

 /**
  * Конструктор
  * @param array $db_config Ассоциативный массив с конфигурацией DB
  * @throws Exception в случае ошибки
  */
 public function __construct($db_config)
 {
  $this->db_config = $db_config;
 }

 public function validate($email, $body)
 {
  $this->check_required($email, $body);
  $this->check_email_exist($email);
  $this->check_email($email);
  $this->check_email_exist($email);
  $this->check_body_tags($body);
  
  return TRUE;
 }

 /**
  * Проверяет наличие в таблице Users заданного email'а
  * @param $email
  * @return TRUE в случае успеха проверки
  * @throws Exception в случае ошибки валидации
  */
 public function check_email_exist($email)
 {
  if ($this->get_mysqli()->query("select `id` from `Users` where `email`='".$this->get_mysqli()->real_escape_string($email)."'")->num_rows > 1) throw new Exception('email_exist'); 
  return TRUE;
 }


 
 /**
  * Проверяет, заполнены ли оба поля
  * @param string $email
  * @param string $body
  * @return TRUE в случае успеха проверки
  * @throws Exception в случае ошибки валидации
  */
 protected function check_required($email, $body)
 {
  if (empty($email) or empty($body)) throw new Exception('required');
  return TRUE;
 }

 /**
  * Проверяет email на правильность
  * @param string $email
  * @return TRUE в случае успеха проверки
  * @throws Exception в случае ошибки валидации
  */
 protected function check_email($email)
 {
  if (!preg_match("/^[a-zA-Z0-9_\-.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-.]+$/", $email)) throw new Exception('email_error');
  return TRUE;
 }

 /**
  * Проверяет body на присутствие тегов
  * @param string $body
  * @return TRUE в случае успеха проверки
  * @throws Exception в случае ошибки валидации
  */
 protected function check_body_tags($body)
 {
  if (preg_match("/<[^>]+>/", $body)) throw new Exception('tags_not_permitted');
  return TRUE;
 }

 /**
  * Возвращает объект для работы с БД
  * @return mysqli
  * @throws Exception
  */
 protected function get_mysqli()
 {
  if (is_null($this->mysqli))
  {
   $this->mysqli = @new mysqli($this->db_config['server'], $this->db_config['user'], $this->db_config['pass'], $this->db_config['name']);
   if ($this->mysqli->connect_error) throw new Exception('DB error');
   $this->mysqli->query("set names utf8");
  }
  
  return $this->mysqli;
 }
}