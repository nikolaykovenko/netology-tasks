<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

require_once __DIR__.'/../db_config.php';
require_once __DIR__.'/task_2.php';

try {
 $task = new task_2($db_config);
 $result = $task->exec(__DIR__.'/../results/noimage.txt');
}
catch (Exception $e)
{
 $result = $e->getMessage();
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
 <meta charset="utf-8">
 <title>Тестовое задание 2 - netology</title>
 <style>
  .table {
   width: 100%;
   border-collapse: collapse;
  }

  .table td {
   width: 23%;
   padding: 3px 1%;
   border: 1px solid #000;
  }
 </style>
</head>
<body>
<?php
if (is_array($result))
{
 echo '<table class="table">
        <tr>';
 foreach ($result as $index => $line)
 {
  if ($index > 0 and $index%4 == 0) echo '</tr><tr>';
  echo '<td>'.implode(', ', array($line->id, $line->name, $line->price)).'</td>';
 }
 
 echo '</tr>
       </table>';
}
else echo $result;
?>
</body>
</html>
