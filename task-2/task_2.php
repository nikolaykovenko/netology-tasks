<?php
/**
 * @package test_task
 * @author Nikolay Kovenko <nikolay.kovenko@gmail.com>
 * @date 07.04.14
 */

/**
 * Решение тестового задания №2
 */
class task_2 {
 /**
  * @var mysqli
  */
 protected $mysqli;

 /**
  * @var resource
  */
 protected $file;

 /**
  * Конструктор
  * @param array $db_config Ассоциативный массив с конфигурацией DB
  * @throws Exception в случае ошибки
  */
 public function __construct($db_config)
 {
  $this->mysqli = @new mysqli($db_config['server'], $db_config['user'], $db_config['pasfs'], $db_config['name']);
  if ($this->mysqli->connect_error) throw new Exception('DB error');
  $this->mysqli->query("set names utf8");
 }

 /**
  * Главный метод для запуска скрипта
  * @param string $export_file файл вывода товаров без изображений
  * @throws Exception
  * @return array Возвращает массив объектов товаров
  */
 public function exec($export_file = 'results/noimage.txt')
 {
  $this->file = @fopen($export_file, 'w');
  if (empty($this->file)) throw new Exception('export error');
  
  $data = $this->mysqli->query("select * from (select * from `Products` order by `id` desc limit 0, 100) as `last_products_desc` order by `id`");
  if (mysqli_error($this->mysqli)) throw new Exception(mysqli_error($this->mysqli));

  $result = array();
  
  while ($line = $data->fetch_object())
  {
   if (empty($line->image)) $this->write_id_to_file($line->id); else $result[] = $line;
  }
  
  fclose($this->file);
  $data->close();
  return $result;
 }

 /**
  * Записывает ID в файл для экспорта
  * @param int $id
  * @return int
  * @throws Exception
  */
 protected function write_id_to_file($id)
 {
  $result = fwrite($this->file, $id."\n");
  if ($result === FALSE) throw new Exception('export error');
  return $result;
 }
 
}